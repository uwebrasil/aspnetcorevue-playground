'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

//module.exports = merge(prodEnv, {
//  NODE_ENV: '"development"'
//})

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  //API_ENDPOINT: '"http://localhost:44336/api"'
  //API_ENDPOINT: '"http://localhost:5000/api"'
  //API_ENDPOINT: '"https://api.mocki.io/v1/b043df5a"'
  //API_ENDPOINT: '"https://localhost:44336/api/token/tokensuperuser"'
  API_ENDPOINT: '"https://localhost:44336/api/merchants/getaddress?idMerchant=1"'

})
